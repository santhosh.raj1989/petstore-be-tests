@PetStoreitems
Feature: Pet Store content API Tests

  As a user of Petstore API's,

  I should be able verify Pet store items Restful API's which will include the creating
  a new pet content, updating existing pet content and deleting the pet content

  Scenario:Verification of create a new pet store item
    When I send Post request for new pet
      | id | categoryid | categoryname | name       | photoUrls | tagid | tagname | status    |
      | 3  | 6          | doggie1      | doggiepic1 | yes       | true  | testtag | available |
    Then pet Post response is according to Json Schema

  Scenario:Verification of store order content an pet id parameter
    When I send Post request for new pet
      | id | categoryid | categoryname | name       | photoUrls | tagid | tagname | status    |
      | 4  | 8          | doggie2      | doggiepic2 | yes       | true  | testtag | available |
    When I send Get request for pet ID <4>
    Then Get Pet response is according to Json Schema

  Scenario:Verification of store order content update
    When I send Post request for new pet
      | id | categoryid | categoryname | name       | photoUrls | tagid | tagname | status    |
      | 10 | 9          | doggie4      | doggiepic2 | yes       | true  | testtag | available |
    When I send Put request for new pet
      | id | categoryid | categoryname | name       | photoUrls | tagid | tagname | status    |
      | 33 | 3          | doggie8      | doggiepic2 | yes       | true  | testtag | available |
    Then PUT response for pet is according to Json Schema

  Scenario:Verification of deletion of existing order
    When I send Post request for new pet
      | id | categoryid | categoryname | name       | photoUrls | tagid | tagname | status    |
      | 9  | 2          | doggie3      | doggiepic3 | yes       | true  | testtag | available |
    Then I send Delete request with pet id <9>
    Then Delete pet response is according to Json Schema
