@PetStoreOrders
Feature: Pet Store Orders REST API Tests

  As a user of Petstore API's,

  I should be able verify Pet store order related operations which will include creating a new order, fetching
  the order based on order id and deleting an existing order

  Scenario: Verification of create a new pet store order
    When I send Post request for new pet store order
      | id | petId | quantity | shipDate                 | status | complete |
      | 4  | 8     | 4        | 2021-09-26T05:15:43.101Z | placed | true     |
    Then Order Post response is according to Json Schema

  Scenario: Verification of fetching existing pet order details using order id
    Given I send Post request for new pet store order
      | id | petId | quantity | shipDate                 | status | complete |
      | 5  | 6     | 7        | 2021-09-29T05:15:43.101Z | placed | true     |
    When I send Get request for order <5>
    Then Get Order response is according to Json Schema

  Scenario:Verification of deletion of existing order
    Given I send Post request for new pet store order
      | id | petId | quantity | shipDate                 | status | complete |
      | 8  | 6     | 7        | 2021-09-29T05:15:43.101Z | placed | true     |
    When I delete the order "8"
    Then Delete Order response is according to Json Schema
