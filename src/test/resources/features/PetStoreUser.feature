@PetStoreUsers
Feature: Pet Store User Rest API Tests

  As a user of Petstore API's,

  I should be able verify Pet store user operation Restful API's which will include the Login,Logout
  creating a new user, updating the created user information and deleting the created users

  Scenario:Verification of Creating a new user
    When I send Post request for user
      | id | username    | firstname       | lastname        | email                | password    | phone    | userstatus |
      | 4  | newpetuser5 | newpetuserfname | newpetuserlname | newpetuser@email.com | pet3pet$234 | 48737384 | 8          |
    Then Post response is according to Json Schema

  Scenario: Verification of fetching user details with username parameter
    Given A valid pet user exists with below details
      | id | username    | firstname    | lastname         | email                 | password       | phone   | userstatus |
      | 6  | petuserDemo | petuserfname | Testpetuserlname | testpetuser@email.com | petpettest$234 | 4877384 | 9          |
    When I send Get request for username
      | username    |
      | petuserDemo |
    Then Get username response is according to Json Schema

  Scenario: Verification of updating an existing user details
    Given A valid pet user exists with below details
      | id | username     | firstname    | lastname         | email                 | password       | phone   | userstatus |
      | 2  | petuser2Demo | petuserfname | Testpetuserlname | testpetuser@email.com | petpettest$234 | 4877384 | 0          |
    When I send Put request for user
      | id | username     | firstname | lastname           | email                   | password      | phone   | userstatus |
      | 3  | petuser3Demo | petuser4  | petuserlnameupdate | petuserupdate@email.com | petpettest$32 | 4877384 | 0          |
    Then Put response is according to Json Schema

  Scenario:Verification of deleting a user
    Given A valid pet user exists with below details
      | id | username      | firstname       | lastname        | email                    | password       | phone   | userstatus |
      | 9  | petuserDelete | petuserdelfname | petuserdellname | testpetuserdel@email.com | petpettest$234 | 4877384 | 0          |
    When I delete the pet user "petuserDelete"
    Then Delete response is according to Json Schema

  Scenario: Verification of User Login
    When I send Get request for user login
    Then Get response for login is according to Json Schema

  Scenario: Verification of User Logout
    When I send Get request for user logout
    Then Get response for logout is according to Json Schema




