package PageObjects;

import Endpoints.OrderEndpoints;
import Utilities.PropertiesReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class OrderCaller {

    public Response getOrderName(int orderId) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().get(PropertiesReader.
                getValue("baseUrl") + OrderEndpoints.READORDER.getUrl() + orderId);
    }

    public Response postCreateOrder(List<Map<String, String>>
                                            postorderInfo) throws Exception {
        String requestBody = "{\n" +
                "  \"id\": \"" + postorderInfo.get(0).get("id") + "\",\n" +
                "  \"petId\": \"" + postorderInfo.get(0).get("petId") + "\",\n" +
                "  \"quantity\":\"" + postorderInfo.get(0).get("quantity") + "\",\n" +
                "  \"shipDate\": \"" + postorderInfo.get(0).get("shipDate") + "\",\n" +
                "  \"status\": \"" + postorderInfo.get(0).get("status") + "\",\n" +
                "  \"complete\": true}";
        return given().log().all().contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(requestBody).
                when().
                post(PropertiesReader.
                        getValue("baseUrl") + OrderEndpoints.CREATEORDER.getUrl());
    }

    public Response deleteorderName(String orderID) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().delete(PropertiesReader.
                getValue("baseUrl") + OrderEndpoints.DELETEORDER.getUrl() + orderID);
    }

    public void ordernameGetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/orders/getOrdernameSchema.json"));
    }

    public void orderNamePostResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/orders/postOrdernameSchema.json"));
    }

    public void deleteOrderResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/orders/deleteOrderSchema.json"));
    }
}