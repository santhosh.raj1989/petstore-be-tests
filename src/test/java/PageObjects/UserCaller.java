package PageObjects;

import Utilities.PropertiesReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import Endpoints.UserEndpoints;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class UserCaller {

    private Map<String, String> prepareDefaultParameters() {
        HashMap map = new HashMap();
        map.put("username", "user1");
        map.put("password", "password");
        return map;
    }

    public Response userLogin() throws Exception {
        Map<String, String> parameters = prepareDefaultParameters();
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().
                params(parameters).get(PropertiesReader.getValue("baseUrl") +
                UserEndpoints.LOGIN.getUrl());
    }

    public Response userLogout() throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().get(PropertiesReader.
                getValue("baseUrl") + UserEndpoints.LOGOUT.getUrl());
    }

    public Response getUserName(String userName) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().get(PropertiesReader.
                getValue("baseUrl") + UserEndpoints.USERINFO.getUrl() + userName);
    }

    public Response postUserName(List<Map<String, String>>
                                         postUserInfo) throws Exception {
        String requestBody = "{\n" +
                "  \"id\": \"" + postUserInfo.get(0).get("id") + "\",\n" +
                "  \"username\": \"" + postUserInfo.get(0).get("username") + "\",\n" +
                 "  \"firstName\": \"" + postUserInfo.get(0).get("firstname") + "\",\n" +
                "  \"lastName\": \"" + postUserInfo.get(0).get("lastname") + "\",\n" +
                 "  \"email\": \"" + postUserInfo.get(0).get("email") + "\",\n" +
                "  \"password\": \"" + postUserInfo.get(0).get("password") + "\",\n" +
                 "  \"phone\": \"" + postUserInfo.get(0).get("phone") + "\",\n" +
                "  \"userStatus\": \"" + postUserInfo.get(0).get("userstatus") + "\" }";
        return given().log().all().contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(requestBody).
                when().
                post(PropertiesReader.
                        getValue("baseUrl") + UserEndpoints.CREATEUSER.getUrl());
    }

    public Response putUserName(List<Map<String, String>>
                                        putUserInfo) throws Exception {
        String requestBody = "{\n" +
                "  \"id\": \"" + putUserInfo.get(0).get("id") + "\",\n" +
                "  \"username\": \"" + putUserInfo.get(0).get("username") + "\",\n" +
                "  \"firstName\": \"" + putUserInfo.get(0).get("firstname") + "\",\n" +
                "  \"lastName\": \"" + putUserInfo.get(0).get("lastname") + "\",\n" +
                "  \"email\": \"" + putUserInfo.get(0).get("email") + "\",\n" +
                "  \"password\": \"" + putUserInfo.get(0).get("password") + "\",\n" +
                "  \"phone\": \"" + putUserInfo.get(0).get("phone") + "\",\n" +
                "  \"userStatus\": \"" + putUserInfo.get(0).get("userstatus") + "\" }";
        return given().log().all().contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(requestBody).
                when().
                put(PropertiesReader.
                        getValue("baseUrl") + UserEndpoints.UPDATEUSER.getUrl());
    }

    public Response deleteUserName(String userName) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().delete(PropertiesReader.
                getValue("baseUrl") + UserEndpoints.DELETEUSER.getUrl() + userName);
    }

    public void UsernameGetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().
                body(matchesJsonSchemaInClasspath("jsonSchemas/users/getUsernameSchema.json"));
    }
    public void usernamePostResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().
                body(matchesJsonSchemaInClasspath("jsonSchemas/users/postUsernameSchema.json"));
    }
    public void checkLoginGetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().
                body(matchesJsonSchemaInClasspath("jsonSchemas/users/loginSchema.json"));
    }
    public void checkLogoutGetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().
                body(matchesJsonSchemaInClasspath("jsonSchemas/users/logoutSchema.json"));
    }
}