package PageObjects;

import Endpoints.PetEndpoints;
import Utilities.PropertiesReader;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;

public class PetCaller {
    PropertiesReader propertiesReader = new PropertiesReader();

    private Map<String, String> prepareDefaultParameters() {
        Map<String, String> map = new HashMap();
        map.put("Petname", "Pet1");
        map.put("password", "password");
        return map;
    }

    public Response getPetName(int PetId) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().get(PropertiesReader.
                getValue("baseUrl") + PetEndpoints.READPET.getUrl() + PetId);
    }

    public Response postCreatePet(List<Map<String, String>>
                                          postPetInfo) throws Exception {
        String requestBody = "{\"id\": \"" + postPetInfo.get(0).get("id") + "\"," +
                "\"category\":{\"id\": \"" + postPetInfo.get(0).get("categoryid") + "\",\n" +
                "\"name\":\"" + postPetInfo.get(0).get("name") + "\"}," +
                "\"name\":" + "\"doggie\"," +
                "\"photoUrls\":[\"string\"]," +
                "\"tags\":[{\"id\":0,\"name\":\"string\"}]," +
                "\"status\":\"available\"}";

        return given().log().all().contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(requestBody).
                when().
                post(PropertiesReader.
                        getValue("baseUrl") + PetEndpoints.CREATEPET.getUrl());
    }

    public Response updatePet(List<Map<String, String>>
                                          postPetInfo) throws Exception {
        String requestBody = "{\"id\": \"" + postPetInfo.get(0).get("id") + "\"," +
                "\"category\":{\"id\": \"" + postPetInfo.get(0).get("categoryid") + "\",\n" +
                "\"name\":\"" + postPetInfo.get(0).get("name") + "\"}," +
                "\"name\":" + "\"doggie\"," +
                "\"photoUrls\":[\"string\"]," +
                "\"tags\":[{\"id\":0,\"name\":\"string\"}]," +
                "\"status\":\"available\"}";

        return given().log().all().contentType(ContentType.JSON).
                accept(ContentType.JSON).
                body(requestBody).
                when().
                post(PropertiesReader.
                        getValue("baseUrl") + PetEndpoints.UPDATEPET.getUrl());
    }

    public Response deletePetName(int PetID) throws Exception {
        return given().log().all().
                contentType(ContentType.JSON).
                accept(ContentType.JSON).
                relaxedHTTPSValidation().delete(PropertiesReader.
                getValue("baseUrl") + PetEndpoints.DELETEPET.getUrl() + PetID);
    }

    public void PetnameGetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/pet/getPetSchema.json"));
    }

    public void petPostResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/pet/postPetSchema.json"));
    }

    public void petPutResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/pet/putPetSchema.json"));
    }

    public void deletePetResponse(Response response) {
        response.then().
                statusCode(HttpURLConnection.HTTP_OK).
                and().
                assertThat().body(matchesJsonSchemaInClasspath("jsonSchemas/pet/deletePetSchema.json"));
    }
}