package Endpoints;

public enum PetEndpoints {

    CREATEPET("create Pet", "/pet"),
    READPET("create Pet", "/pet/"),
    DELETEPET("delete Pet information", "/pet/"),
    UPDATEPET("update Pet information", "/pet/");

    private final String description;
    private final String url;

    PetEndpoints(String description, String url) {
        this.description = description;
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
