package Endpoints;

public enum OrderEndpoints {

    CREATEORDER("create order", "/store/order"),
    READORDER("read order information", "/store/order/"),
    DELETEORDER("delete order information", "/store/order/");

    private final String url;

    OrderEndpoints(String description, String url) {
        this.url = url;
    }
    public String getUrl() {
        return url;
    }
}
