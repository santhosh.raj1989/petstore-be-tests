package Endpoints;

public enum UserEndpoints {

    LOGIN("login", "/user/login"),
    LOGOUT("logout", "/user/logout"),
    USERINFO("user information", "/user/"),
    CREATEUSER("create user", "/user"),
    UPDATEUSER("update user information", "/user/test"),
    DELETEUSER("delete user information", "/user/"),
    CREATEUSERARRAY("delete user information", "/createWithArray");

    private final String url;

    UserEndpoints(String description, String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
