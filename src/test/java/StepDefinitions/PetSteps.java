package StepDefinitions;

import PageObjects.PetCaller;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;


public class PetSteps {
    Response petCallerResponse;
    PetCaller petCaller = new PetCaller();

    // *** When *** //

    @When("^I send Post request for new pet$")
    public void iSendPostRequestForNewPet(List<Map<String, String>> petInfo) throws Exception {
        petCallerResponse = petCaller.postCreatePet(petInfo);
        Assert.assertNotNull(petCallerResponse);
    }

    @When("^I send Get request for pet ID <(\\d+)>$")
    public void iSendGetRequestForPet(int petId) throws Exception {
        petCallerResponse = petCaller.getPetName(petId);
        Assert.assertNotNull(petCallerResponse);
    }

    @When("^I send Delete request with pet id <(\\d+)>$")
    public void iSendDeleteRequestWithPetId(int petId) throws Exception {
        petCallerResponse = petCaller.deletePetName(petId);
        Assert.assertNotNull(petCallerResponse);
    }

    @When("^I send Put request for new pet$")
    public void iSendPutRequestForNewPet(List<Map<String, String>> petInfo) throws Exception {
        petCallerResponse = petCaller.updatePet(petInfo);
        Assert.assertNotNull(petCallerResponse);
    }

    // *** Then *** //

    @Then("^Get Pet response is according to Json Schema$")
    public void getPetResponseIsAccordingToJsonSchema() {
        petCaller.PetnameGetResponse(petCallerResponse);
    }

    @Then("^Delete pet response is according to Json Schema$")
    public void deleteResponseIsAccordingToJsonSchema() {
        petCaller.deletePetResponse(petCallerResponse);
    }

    @Then("^pet Post response is according to Json Schema$")
    public void postResponseIsAccordingToJsonSchema() {
        petCaller.petPostResponse(petCallerResponse);
    }

    @Then("^PUT response for pet is according to Json Schema$")
    public void putResponseForPetIsAccordingToJsonSchema() {
        petCaller.petPutResponse(petCallerResponse);
    }
}
