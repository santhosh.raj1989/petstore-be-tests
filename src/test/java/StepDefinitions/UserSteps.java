package StepDefinitions;

import PageObjects.UserCaller;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;


public class UserSteps {
    Response userCallerResponse;
    UserCaller userCaller = new UserCaller();

    //*** Given ***

    @Given("^A valid pet user exists with below details$")
    public void aValidPetUserExistsWithBelowDetails(List<Map<String, String>> postUserInfo)
            throws Exception {
        userCallerResponse = userCaller.postUserName(postUserInfo);
        Assert.assertNotNull(userCallerResponse);
        userCaller.usernamePostResponse(userCallerResponse);
    }

    //*** When ***

    @When("^I send Get request for user login$")
    public void iSendGetRequestForUserLogin() throws Exception {
        userCallerResponse = userCaller.userLogin();
        Assert.assertNotNull(userCallerResponse);
    }

    @When("^I send Get request for user logout$")
    public void iSendGetRequestForUserLogout() throws Exception {
        userCallerResponse = userCaller.userLogout();
        Assert.assertNotNull(userCallerResponse);
    }

    @When("^I send Get request for username$")
    public void iSendGetRequestForUsername(DataTable userNameData) throws Exception {
        List<String> userName = userNameData.asList(String.class);
        userCallerResponse = userCaller.getUserName(userName.get(1));
        Assert.assertNotNull(userCallerResponse);
    }

    @When("^I send Post request for user$")
    public void iSendPostRequestForUser(List<Map<String, String>> postUserInfo) throws Exception {
        userCallerResponse = userCaller.postUserName(postUserInfo);
        Assert.assertNotNull(userCallerResponse);
    }
    @When("^I send Put request for user$")
    public void iSendPutRequestForUser(List<Map<String, String>> putUserName) throws Exception {
        userCallerResponse = userCaller.putUserName(putUserName);
        Assert.assertNotNull(userCallerResponse);
    }

    @When("^I delete the pet user \"([^\"]*)\"$")
    public void iDeleteThePetUser(String userName) throws Throwable {
        userCallerResponse = userCaller.deleteUserName(userName);
        Assert.assertNotNull(userCallerResponse);
    }

    //*** Then ***

    @Then("^Get response for login is according to Json Schema$")
    public void getResponseForLoginIsAccordingToJsonSchema() {
        userCaller.checkLoginGetResponse(userCallerResponse);
    }

    @Then("^Get response for logout is according to Json Schema$")
    public void getResponseForLogoutIsAccordingToJsonSchema() {
        userCaller.checkLogoutGetResponse(userCallerResponse);
    }

    @Then("^Get username response is according to Json Schema$")
    public void getUsernameResponseIsAccordingToJsonSchema() {
        userCaller.UsernameGetResponse(userCallerResponse);
    }

    @Then("^Post response is according to Json Schema$")
    public void postResponseIsAccordingToJsonSchema() {
        userCaller.usernamePostResponse(userCallerResponse);
    }

    @Then("^Put response is according to Json Schema$")
    public void putResponseIsAccordingToJsonSchema() {
        userCaller.usernamePostResponse(userCallerResponse);
    }

    @Then("^Delete response is according to Json Schema$")
    public void deleteResponseIsAccordingToJsonSchema() {
        userCaller.usernamePostResponse(userCallerResponse);
    }

}