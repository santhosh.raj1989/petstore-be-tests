package StepDefinitions;

import PageObjects.OrderCaller;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;

import java.util.List;
import java.util.Map;


public class PetStoreOrderSteps {
    Response orderCallerResponse;
    OrderCaller orderCaller = new OrderCaller();

    //*** When ***

    @When("^I send Post request for new pet store order$")
    public void iSendPostRequestForUser(List<Map<String, String>> orderInfo) throws Exception {
        orderCallerResponse = orderCaller.postCreateOrder(orderInfo);
        Assert.assertNotNull(orderCallerResponse);
    }

    @When("^I send Get request for order <(\\d+)>$")
    public void iSendGetRequestForOrder(int orderId) throws Exception {
        orderCallerResponse = orderCaller.getOrderName(orderId);
        Assert.assertNotNull(orderCallerResponse);
    }

    @When("^I delete the order \"([^\"]*)\"$")
    public void iDeleteThePetOrder(String orderId) throws Throwable {
        orderCallerResponse = orderCaller.deleteorderName(orderId);
        Assert.assertNotNull(orderCallerResponse);
    }

    //*** Then ***

    @Then("^Order Post response is according to Json Schema$")
    public void postResponseIsAccordingToJsonSchema() {
        orderCaller.orderNamePostResponse(orderCallerResponse);
    }

    @Then("^Get Order response is according to Json Schema$")
    public void getOrderResponseIsAccordingToJsonSchema() {
        orderCaller.ordernameGetResponse(orderCallerResponse);
    }

    @Then("^Delete Order response is according to Json Schema$")
    public void deleteResponseIsAccordingToJsonSchema() {
        orderCaller.deleteOrderResponse(orderCallerResponse);
    }
}
